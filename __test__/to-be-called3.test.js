


const called = require("../src/called");
// import called from '../src/called';

describe('Stub',()=>{

    test('stub .toBeCalled()', () => {
        const stub = jest.fn();
        stub();
        expect(stub).toBeCalled();
    });
    test('spyOn .toBeCalled()', () => {
        const somethingSpy = jest.spyOn(called, 'child');
        called.parent();
        expect(somethingSpy).toBeCalled();
    });
    test('stub child', () => {
        called.child = jest.fn().mockImplementation(() => {console.log('does stub child');});
        called.parent();
        expect(called.child).toBeCalled();
    });

});